import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'util.dart';

const site = 'https://api.hgbrasil.com/finance?format=json&key=643a8f34';

void main() async {
  // print(await pegarDados());

  runApp(MaterialApp(
    home: Home(),
    theme: ThemeData(hintColor: colorMain, primaryColor: colorMain),
    debugShowCheckedModeBanner: false,
  ));
}

Future<Map> pegarDados() async {
  http.Response resposta = await http.get(site);
  return json.decode(resposta.body);
  // print(json.decode(resposta.body)["results"]["currencies"]["USD"]);
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  double euro;
  double dolar;

  final realControle = TextEditingController();
  final euroControle = TextEditingController();
  final dolarControle = TextEditingController();

  void _realMudar(String x) {
    double inputReal = double.parse(x);
    dolarControle.text = (inputReal / dolar).toStringAsFixed(2);
    euroControle.text = (inputReal / euro).toStringAsFixed(2);
  }

  void _dolarMudar(String x) {
    double inputDolar = double.parse(x);
    realControle.text = (dolar * inputDolar).toStringAsFixed(2);
    euroControle.text = (dolar * inputDolar / euro).toStringAsFixed(2);
  }

  void _euroMudar(String x) {
    double inputEuro = double.parse(x);
    realControle.text = (euro * inputEuro).toStringAsFixed(2);
    dolarControle.text = (euro * inputEuro / dolar).toStringAsFixed(2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text('Conversor de moedas'),
          centerTitle: true,
          backgroundColor: colorMain,
        ),
        body: FutureBuilder<Map>(
            future: pegarDados(),
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                case ConnectionState.waiting:
                  return textoCarregando('Carregando...');
                default:
                  if (snapshot.hasError) {
                    return textoCarregando('Erro ao carregar dados');
                  } else {
                    dolar =
                        snapshot.data["results"]["currencies"]["USD"]["buy"];
                    euro = snapshot.data["results"]["currencies"]["EUR"]["buy"];

                    return SingleChildScrollView(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.monetization_on,
                            size: 150.0,
                            color: colorMain,
                          ),
                          textFiel('R\$ ', 'Reais', realControle, _realMudar),
                          Divider(),
                          textFiel(
                              'US\$ ', 'Dólares', dolarControle, _dolarMudar),
                          Divider(),
                          textFiel('£ ', 'Euros', euroControle, _euroMudar),
                        ],
                      ),
                    );
                  }
              }
            }));
  }
}
